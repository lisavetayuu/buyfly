package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.CityDao;
import ua.cn.stu.tpps.buyfly.domain.City;
import ua.cn.stu.tpps.buyfly.services.CityService;

import java.util.Collection;

/**
 * Implementation of CityService
 */
@Transactional
public class CityServiceImpl extends GenericServiceImpl<City, CityDao> implements CityService {

    @Autowired
    protected void setAirlineDao(CityDao cityDao){
        dao = cityDao;
    }

    @Override
    public City getCity(String cityName) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<City> getCitiesByCountry(String countryName) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<City> getCitiesByAirline(String airlineName) {
        //TODO Implement it
        return null;
    }
}
