package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.BoardingPassDao;
import ua.cn.stu.tpps.buyfly.domain.BoardingPass;
import ua.cn.stu.tpps.buyfly.services.BoardingPassService;

import java.util.Collection;

@Transactional
public class BoardingPassServiceImpl extends GenericServiceImpl<BoardingPass, BoardingPassDao> implements BoardingPassService {

    @Autowired
    protected void setAirlineDao(BoardingPassDao boardingPassDao){
        dao = boardingPassDao;
    }

    @Override
    public Collection<BoardingPass> getByCustomer(Integer customerId) {
        //TODO Implement it
        return null;
    }

    @Override
    public boolean cancelBooking(Integer boardingPassId) {
        //TODO Implement it
        return false;
    }

    @Override
    public BoardingPass getBookedByCustomer(Integer customerId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<BoardingPass> getByStatus(String status) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<BoardingPass> getByStatusAndFlight(Integer flightId, String status) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<BoardingPass> getByFlight(Integer flightId) {
        //TODO Implement it
        return null;
    }
}
