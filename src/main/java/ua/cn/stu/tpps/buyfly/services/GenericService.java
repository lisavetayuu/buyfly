package ua.cn.stu.tpps.buyfly.services;

import java.util.Collection;

/**
 * Contains similar actions like save, remove, getById, getAll ...
 */
public interface GenericService<T> {
    T save(T entity) throws Exception;

    void remove(T entity) throws Exception;

    Collection<T> getAll() throws Exception;

    T getById(Integer id) throws Exception;
}
