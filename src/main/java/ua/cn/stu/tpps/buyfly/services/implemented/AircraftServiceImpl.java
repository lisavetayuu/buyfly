package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.AircraftDao;
import ua.cn.stu.tpps.buyfly.domain.Aircraft;
import ua.cn.stu.tpps.buyfly.domain.Flight;
import ua.cn.stu.tpps.buyfly.services.AircraftService;
import ua.cn.stu.tpps.buyfly.util.ResourceMessage;

import java.util.Collection;

/**
 * Implementation of AircraftService
 */
@Transactional
public class AircraftServiceImpl extends GenericServiceImpl<Aircraft, AircraftDao> implements AircraftService {

    private static final String BUNDLE_EXCEPTION_MESSAGES = "errormessages";

    @Autowired
    protected void setAircraftDao(AircraftDao aircraftDao) {
        dao = aircraftDao;
    }

    @Override
    public Collection<Flight> getFlightsForAircraft(Integer aircraftId) throws Exception {
        try {
            return dao.getFlightsForAircraft(aircraftId);
        } catch (Exception e) {
            throw new Exception(ResourceMessage.get(BUNDLE_EXCEPTION_MESSAGES, "flight.noForAircraft", aircraftId), e);
        }
    }
}
