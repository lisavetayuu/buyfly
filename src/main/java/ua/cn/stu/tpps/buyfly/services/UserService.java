package ua.cn.stu.tpps.buyfly.services;

import ua.cn.stu.tpps.buyfly.domain.User;

import java.util.Collection;

public interface UserService {
    boolean checkExists(User user);

    void setEnabled(boolean enabled);

    void sendConfirmationEmail(User user);

    boolean login(String login, String password);

    Collection<User> getByRoleType(String role);

    Collection<User> getByFlight(Integer flightId);

    User getByEmail(String email);
}
