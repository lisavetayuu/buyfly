package ua.cn.stu.tpps.buyfly.services.implemented;

import ua.cn.stu.tpps.buyfly.dao.GenericDao;
import ua.cn.stu.tpps.buyfly.domain.DomainSuperClass;
import ua.cn.stu.tpps.buyfly.services.GenericService;
import ua.cn.stu.tpps.buyfly.util.ResourceMessage;

import java.util.Collection;

/**
 * Implements all the methods of the interface {@link GenericService}, using
 * methods of {@link GenericDao} for operations with dao.
 * This class defines several methods that must be implemented in subclasses. These are methods that update
 * object with new information and initialize the interface {@link GenericDao}.
 *
 * @param <T> class of object to work with
 */
public abstract class GenericServiceImpl<T extends DomainSuperClass, TDao extends GenericDao<T>> implements GenericService<T> {

    private static final String BUNDLE_EXCEPTION_MESSAGES = "errormessages";

    // Interface to work with
    protected TDao dao;

    public GenericServiceImpl() {
        super();
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.services.GenericService#save(java.lang.Object)
     */
    @Override
    public T save(T entity) throws Exception {
        try {
            T newEntity = dao.save(entity);

            if (newEntity != null) {
                return newEntity;
            } else {
                throw new Exception(ResourceMessage
                    .get(BUNDLE_EXCEPTION_MESSAGES, "entity.failedTo.add", entity.getClass().getSimpleName()));
            }
        } catch (Exception e) {
            throw new Exception(ResourceMessage
                .get(BUNDLE_EXCEPTION_MESSAGES, "entity.failedTo.add", entity.getClass().getSimpleName()), e);
        }
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.services.GenericService#remove(java.lang.Object)
     */
    @Override
    public void remove(T entity) throws Exception {
        try {
            dao.remove(entity);
        } catch (Exception e) {
            throw new Exception(ResourceMessage
                .get(BUNDLE_EXCEPTION_MESSAGES, "entity.failedTo.remove", entity.getClass().getSimpleName()), e);
        }
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.services.GenericService#getAll()
     */
    @Override
    public Collection<T> getAll() throws Exception {
        try {
            return dao.getAll();
        } catch (Exception e) {
            throw new Exception(ResourceMessage.get(BUNDLE_EXCEPTION_MESSAGES, "entity.failedTo.getAll"), e);
        }
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.services.GenericService#getById(java.lang.Integer)
     */
    @Override
    public T getById(Integer id) throws Exception {
        try {
            T savedEntity = dao.getById(id);

            if ((savedEntity == null) || (savedEntity.getId() == null)) {
                throw new Exception(ResourceMessage.get(BUNDLE_EXCEPTION_MESSAGES, "entity.incorrectId", id));
            }

            return savedEntity;
        } catch (Exception e) {
            throw new Exception(ResourceMessage.get(BUNDLE_EXCEPTION_MESSAGES, "entity.failedTo.getWithId", id), e);
        }
    }
}
