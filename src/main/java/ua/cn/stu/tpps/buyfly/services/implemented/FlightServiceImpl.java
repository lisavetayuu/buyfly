package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.FlightDao;
import ua.cn.stu.tpps.buyfly.domain.Flight;
import ua.cn.stu.tpps.buyfly.services.FlightService;

import java.time.LocalDateTime;
import java.util.Collection;

@Transactional
public class FlightServiceImpl extends GenericServiceImpl<Flight, FlightDao> implements FlightService {

    @Autowired
    protected void setAirlineDao(FlightDao flightDao){
        dao = flightDao;
    }

    @Override
    public Collection<Flight> getByDirection(Integer originCityId, Integer destinationCityId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getBetweenDates(LocalDateTime departure, LocalDateTime arrival) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getByAircraft(Integer aircraftId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getByDatesAndAircraft(LocalDateTime departure, LocalDateTime arrival, Integer aircraftId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getByCountries(String origin, String destination) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getByAirports(Integer originAirportId, Integer destinationAirportId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getCurrentFlightsByCustomer(Integer customerId) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Flight> getPreviousFlightsByCustomer(Integer customerId) {
        //TODO Implement it
        return null;
    }
}
