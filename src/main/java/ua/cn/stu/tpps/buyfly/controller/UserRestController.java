package ua.cn.stu.tpps.buyfly.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ua.cn.stu.tpps.buyfly.domain.User;
import ua.cn.stu.tpps.buyfly.services.UserService;

import java.util.LinkedList;
import java.util.List;

@RestController
public class UserRestController {

    private UserService userService;  //Service which will do all data retrieval/manipulation work

    @Autowired
    private void setUserService(UserService userService) {
        this.userService = userService;
    }

    //-------------------Retrieve All Users--------------------------------------------------------

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    @ResponseBody
    public ResponseEntity<List<User>> listAllUsers(@RequestParam int flightId) throws Exception {
        List<User> users = new LinkedList<>();

        users.add(new User());
        if (users.isEmpty()) {
            return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);//You many decide to return HttpStatus.NOT_FOUND
        }
        return new ResponseEntity<List<User>>(users, HttpStatus.OK);
    }

}
