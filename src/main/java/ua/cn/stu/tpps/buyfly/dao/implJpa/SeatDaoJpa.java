package ua.cn.stu.tpps.buyfly.dao.implJpa;

import ua.cn.stu.tpps.buyfly.dao.SeatDao;
import ua.cn.stu.tpps.buyfly.domain.Seat;

import javax.persistence.PersistenceException;
import java.util.Collection;

/**
 * Implements Seat DAO.
 */
public class SeatDaoJpa extends GenericDaoJpa<Seat> implements SeatDao {

    public SeatDaoJpa() {
        super(Seat.class);
    }

    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.dao.SeatDao#getByClass(java.lang.String)
     */
    @Override
    public Collection<Seat> getByClass(String seatClass) throws PersistenceException {
        return executeQuery("getByClass", true, false, seatClass);
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.dao.SeatDao#getFreeSeats()
     */
    @Override
    public Collection<Seat> getFreeSeats() throws PersistenceException {
        return executeQuery("getFreeSeats", true, false);
    }


    /**
     * (non-Javadoc)
     *
     * @see ua.cn.stu.tpps.buyfly.dao.SeatDao#getFreeByPriceRange(double, double)
     */
    @Override
    public Collection<Seat> getFreeByPriceRange(double lowestPrice, double highestPrice) throws PersistenceException {
        return executeQuery("getFreeByPriceRange", true, false, lowestPrice, highestPrice);
    }
}
