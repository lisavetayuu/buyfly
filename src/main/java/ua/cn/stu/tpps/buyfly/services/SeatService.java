package ua.cn.stu.tpps.buyfly.services;

import ua.cn.stu.tpps.buyfly.domain.Seat;

import java.util.Collection;

public interface SeatService extends GenericService<Seat> {
    Collection<Seat> getByClass(String seatClass);

    Collection<Seat> getFreeSeats();

    Collection<Seat> getFreeByPriceRange(double lowestPrice, double highestPrice);
}
