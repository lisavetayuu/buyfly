package ua.cn.stu.tpps.buyfly.dao;

import ua.cn.stu.tpps.buyfly.domain.Seat;

import javax.persistence.PersistenceException;
import java.util.Collection;

/**
 * DAO layer interface to work with the class {@link ua.cn.stu.tpps.buyfly.domain.Seat}.
 * Extends a generic interface, adding to it additional functions.
 */
public interface SeatDao extends GenericDao<Seat> {

    /**
     * Get collection of seats by class (fare basis).
     *
     * @param seatClass seat class (fare basis)
     * @return collection of seats
     * @throws PersistenceException if error occurs
     */
    Collection<Seat> getByClass(String seatClass) throws PersistenceException;


    /**
     * Get collection of free seats.
     *
     * @return collection of free seats
     * @throws PersistenceException if error occurs
     */
    Collection<Seat> getFreeSeats() throws PersistenceException;


    /**
     * Get collection of free seats by price range.
     *
     * @param lowestPrice  lower limit of price
     * @param highestPrice upper limit of price
     * @return collection of seats
     * @throws PersistenceException if error occurs
     */
    Collection<Seat> getFreeByPriceRange(double lowestPrice, double highestPrice) throws PersistenceException;
}
