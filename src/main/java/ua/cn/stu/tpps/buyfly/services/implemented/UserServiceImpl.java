package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.UserDao;
import ua.cn.stu.tpps.buyfly.domain.User;
import ua.cn.stu.tpps.buyfly.services.UserService;

import java.util.Collection;

/**
 * Implementation of UserService
 */
@Transactional
public class UserServiceImpl extends GenericServiceImpl<User, UserDao> implements UserService {

    @Autowired
    protected void setAirlineDao(UserDao userDao){
        dao = userDao;
    }

    @Override
    public User getByEmail(String email) {
        return dao.getByEmail(email);
    }

    @Override
    public boolean checkExists(User user) {
        //TODO Implement commented
        //TODO Override equals() for class User
//        User savedUser = dao.getById(user.getId());
//        return savedUser != null && savedUser.equals(user)

        return false;
    }

    @Override
    public void setEnabled(boolean enabled) {
        //TODO Implement it
    }

    @Override
    public void sendConfirmationEmail(User user) {
        //TODO Implement it
    }

    @Override
    public boolean login(String login, String password) {
        //TODO Implement it
        return false;
    }

    @Override
    public Collection<User> getByRoleType(String role) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<User> getByFlight(Integer flightId) {
        //TODO Implement it
        return null;
    }
}
