package ua.cn.stu.tpps.buyfly.services.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ua.cn.stu.tpps.buyfly.dao.SeatDao;
import ua.cn.stu.tpps.buyfly.domain.Seat;
import ua.cn.stu.tpps.buyfly.services.SeatService;

import java.util.Collection;

/**
 * Implementation of SeatService
 */
@Transactional
public class SeatServiceImpl extends GenericServiceImpl<Seat, SeatDao> implements SeatService {

    @Autowired
    protected void setAirlineDao(SeatDao seatDao){
        dao = seatDao;
    }

    @Override
    public Collection<Seat> getByClass(String seatClass) {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Seat> getFreeSeats() {
        //TODO Implement it
        return null;
    }

    @Override
    public Collection<Seat> getFreeByPriceRange(double lowestPrice, double highestPrice) {
        //TODO Implement it
        return null;
    }
}
